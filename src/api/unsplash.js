import axios from 'axios';

export default axios.create({
  baseURL: 'https://api.unsplash.com',
  headers: {
    Authorization:
      'Client-ID d838648eb4ed186cdf086dae014e05cff412deac08b9b9bedf86dab692707fd3'
  }
});
